// ===================THEORY===========================
/*
1. Опишіть своїми словами як працює метод forEach.

Метод forEach з допомогою callBack функції перебирає всі елементи масиву без повернення нового масиву.

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

Методами які мутують массив являються (pop, push, shift, unshift, splice, reverse).
Методи які не мутують массив (slice, concat, map, filter, reduce, sort, reverse).

3. Як можна перевірити, що та чи інша змінна є масивом?

За допомогою оператора typeof, або з допомогою метода Array.isArray(назва змінної);

4. В яких випадках краще використовувати метод map(), а в яких forEach()?

Метод forEach використовується для перебору елементів массиву і все. В той час як метод map переберає елементи массиву з певними маніпуляціями над ними (наприклад перемножити кожен елемент на 2) і ми отримуємо на цій основі новий массив залишивши при цьому старий массив незмінним.
*/
// ===================PRACTICE=========================

// 1.
const strArray = ["travel", "hello", "eat", "ski", "lift"];

let result = strArray.filter((el) => el.length > 3);
console.log(result);

// 2.

const person = [
    {name: "Іван", age: 25, sex: "чоловіча"},
    {name: "Ганна", age: 30, sex: "жіноча"},
    {name: "Степан", age: 15, sex: "чоловіча"},
    {name: "Наталія", age: 20, sex: "жіноча"}
]

let resultTwo = person.filter((key) => key.sex === "чоловіча");

console.log(resultTwo);

// 3.


let arr = [];
function filterBy(array, type){
    arr = new Array(array);
    let resultThree = array.filter((key) => typeof key != type);
    return resultThree;
}
console.log(filterBy(["Hello", 123, "By", true], "string"));


